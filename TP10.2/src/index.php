<?php
require_once('src/salarie.php');

// Création d'un objet Salarie
$salarie1 = new Salarie(12345, "John Doe", 50000.0, "2018-03-15");

// Affichage des informations du salarié
echo $salarie1;

// Teste de la méthode setMatricule()
try {
    $salarie1->setMatricule(67890);
    echo "Nouveau matricule : " . $salarie1->getMatricule() . "\n";
} catch (Exception $e) {
    echo "Erreur : " . $e->getMessage() . "\n";
}

// Teste de la méthode getDateEmbauche()
echo "Date d'embauche : " . $salarie1->getDateEmbauche()->format("Y-m-d") . "\n";

// Teste de la méthode experience()
echo "Expérience : " . $salarie1->experience() . " années\n";

// Teste de la méthode calculerSalaireNet()
echo "Salaire net : " . $salarie1->calculerSalaireNet() . " euros\n";

// Teste de la méthode primeAnnuelle()
echo "Prime annuelle : " . $salarie1->primeAnnuelle() . " euros\n";