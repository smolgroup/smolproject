<?php
// tests/SalarieTest.php

require_once('src/salarie.php');
use PHPUnit\Framework\TestCase;

class testSalarie extends TestCase {

    public function testGetMatricule()
    {
        $salarie = new Salarie();
        $salarie->setMatricule(12345);
        $this->assertEquals(12345, $salarie->getMatricule());
    }
    public function testGetDateEmbauche() {
        $salarie = new Salarie(1, "John Doe", 5000.0, "2022-01-01");
        $this->assertEquals("2022-01-01", $salarie->getDateEmbauche()->format("Y-m-d"));
    }

    public function testExperience() {
        $salarie = new Salarie(1, "John Doe", 5000.0, "2022-01-01");
        $this->assertEquals(2, $salarie->experience());
    }

    public function testCalculerSalaireNet() {
        $salarie = new Salarie(1, "John Doe", 5000.0, "2022-01-01");
        $this->assertEquals(4000.0, $salarie->calculerSalaireNet());
    }

    public function testPrimeAnnuelle()
    {
        $salarie = new Salarie(
            1234,
            "John Doe",
            50000.0,
            "2018-01-01"
        );

        $this->assertEquals(40600, $salarie->primeAnnuelle());
    }
}